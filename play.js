			var play_state = {
                // THE GAME HAS BEEN CREATED
                create: function () {
                    game.stage.backgroundColor = 0xFBF8F1;

                    if(game.device.desktop){
                        game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
                        game.scale.setShowAll();
                        game.scale.minWidth = 320
                        game.scale.minHeight = 420;
                        game.scale.maxWidth = 533; 
                        game.scale.maxHeight = 800; //Make sure these values are proportional to the gameWidth and gameHeight
                        game.scale.pageAlignHorizontally = true;
                        game.scale.pageAlignVertically = true;
                        game.scale.setScreenSize(true);
                    }
                     else {
                        game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
                        game.scale.setShowAll();
                        game.scale.minWidth = 320
                        game.scale.minHeight = 420;
                        //game.scale.maxWidth = 533;
                        //game.scale.maxHeight = 800; //Make sure these values are proportional to the gameWidth and gameHeight
                        game.scale.pageAlignHorizontally = true;
                        game.scale.pageAlignVertically = true;
                        game.scale.forceOrientation(false, true,'turn');
                        game.scale.enterIncorrectOrientation.add(play_state.enterIncorrectOrientation,this);
                        game.scale.leaveIncorrectOrientation.add(play_state.leaveIncorrectOrientation,this);
                        game.scale.setScreenSize(true);


                    }

                    //game.advanceTiming = true ;
                    score_x=235;
                    best_x=330;
                    game.add.sprite(10,200,"grid");
                    fieldArray = new Array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
                    score=0;
                    game.add.button(10, 165, 'restart', play_state.restart);
                    //BEST
                    Best = localStorage.getItem('Best');
                    if(!Best){Best=0};
                    if(Best>9){
                        if(Best<=99){best_x=320};
                        if(Best>99 && Best<1000){best_x=310};
                        if(Best>999 && Best<10000){best_x=300};
                        if(Best>9999 && Best<100000){best_x=290};
                    }
					// listeners for WASD keys and ARROW keys
					upKey = game.input.keyboard.addKey(Phaser.Keyboard.W);
					upKey.onDown.add(play_state.moveUp,this);
    				downKey = game.input.keyboard.addKey(Phaser.Keyboard.S);
    				downKey.onDown.add(play_state.moveDown,this);
    				leftKey = game.input.keyboard.addKey(Phaser.Keyboard.A);
    				leftKey.onDown.add(play_state.moveLeft,this);
    				rightKey = game.input.keyboard.addKey(Phaser.Keyboard.D);
    				rightKey.onDown.add(play_state.moveRight,this);

                    arrow_upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
                    arrow_upKey.onDown.add(play_state.moveUp,this);
                    arrow_downKey = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
                    arrow_downKey.onDown.add(play_state.moveDown,this);
                    arrow_leftKey = game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
                    arrow_leftKey.onDown.add(play_state.moveLeft,this);
                    arrow_rightKey = game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
                    arrow_rightKey.onDown.add(play_state.moveRight,this);                 

    					// sprite group declaration
					tileSprites = game.add.group();
    					// at the beginning of the game we add two "2"
                    var style = { font: "24px Arial", fill: "#645D55" }; 

                    game.add.text(200, 35, "SCORE", style);
                    score_label = this.game.add.text(score_x,65,""+score,{font:"30px Arial",fill:'#645D55'});

                    game.add.text(30,60,"4096",{font: "58px Arial", fill: '#3AA6C0'});

                    game.add.text(307,35,"BEST",style);
                    Best_label = game.add.text(best_x,65,""+Best,{font:"30px Arial",fill:'#645D55',align:"center"});
                    game.add.text(30,120,"Join the numbers and get to the",{font: "12px Arial"});
                    game.add.text(207,120,"4096 tile!",{font:"bold 12px Arial"}); 

                    play_state.addTwo();
                    play_state.addTwo();
				},

                enterIncorrectOrientation: function(event, message){
                        //game.stage.backgroundColor = 0xFFFFFF;
                    console.log('You now have incorrect orientation');
                },
 
 
                leaveIncorrectOrientation: function(event, message){
                    console.log('You now have orientation');
                },

                update: function(){
                    //touch 
                    //console.log("out:"+out);
                    if(game.input.pointer1.isDown){
                        //right
                        if(game.input.activePointer.position.x - game.input.activePointer.positionDown.x > 50 &&
                         game.input.activePointer.duration > 100 && game.input.activePointer.duration < 500){
                            play_state.moveRight();}
                        //down
                        if(game.input.activePointer.position.y - game.input.activePointer.positionDown.y > 50 &&
                         game.input.activePointer.duration > 100 && game.input.activePointer.duration < 500){
                            play_state.moveDown();}
                        //left
                        if(game.input.activePointer.position.x - game.input.activePointer.positionDown.x <- 50 &&
                         game.input.activePointer.duration > 100 && game.input.activePointer.duration < 500){
                            play_state.moveLeft();}
                        //up
                        if(game.input.activePointer.position.y - game.input.activePointer.positionDown.y < - 50 &&
                         game.input.activePointer.duration > 100 && game.input.activePointer.duration < 500){
                            play_state.moveUp();}

                    }
                },
				
				// A NEW "2" IS ADDED TO THE GAME
				addTwo: function (){
					// choosing an empty tile in the field
					do{
						var randomValue = Math.floor(Math.random()*16);
					} while (fieldArray[randomValue]!=0)

                    //console.log(fieldArray[0],fieldArray[1],fieldArray[2],fieldArray[3],fieldArray[4],fieldArray[8]);
					// such empty tile now takes "2" value
					fieldArray[randomValue]=2;
					// creation of a new sprite with "tile" instance, that is "tile.png" we loaded before
					tile = game.add.sprite(play_state.toCol(randomValue)*tileSize+17,play_state.toRow(randomValue)*tileSize+210,"tile");
					// creation of a custom property "pos" and assigning it the index of the newly added "2"
					tile.pos = randomValue;
					// at the beginning the tile is completely transparent
					tile.alpha=0;
					// creation of a text which will represent the value of the tile
					var text = game.add.text(tileSize/2-5,tileSize/2-3,"2",{font:"bold 18px Arial",align:"right"});
                         // setting text anchor in the horizontal and vertical center
					text.anchor.set(0.5);
					// adding the text as a child of tile sprite
					tile.addChild(text);
					// adding tile sprites to the group
					tileSprites.add(tile);
					// creation of a new tween for the tile sprite
					var fadeIn = game.add.tween(tile);
					// the tween will make the sprite completely opaque in 250 milliseconds
					fadeIn.to({alpha:1},250);
					// tween callback
					fadeIn.onComplete.add(function(){
						// updating tile numbers. This is not necessary the 1st time, anyway
						play_state.updateNumbers(); 
						// now I can move
						canMove=true;
					})
                        //gameover
                        var a=0;
                        for(i=0;i<16;i++){
                            if (fieldArray[i]!=0){
                                a=a+1;
                            }}
                        //console.log("a:"+a);   
                        if(a==16){
                                var b=0;
                                var test = new Array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
                                if(fieldArray[0]!=fieldArray[1] && fieldArray[0]!=fieldArray[4] &&fieldArray[0]!=0){b=b+1;test[0]=1}else{test[0]=0;}
                                if(fieldArray[1]!=fieldArray[2] && fieldArray[1]!=fieldArray[5] && fieldArray[1]!=fieldArray[0] &&fieldArray[1]!=0){b=b+1;test[1]=1}else{test[1]=0;}
                                if(fieldArray[2]!=fieldArray[3] && fieldArray[2]!=fieldArray[6] && fieldArray[2]!=fieldArray[1] &&fieldArray[2]!=0){b=b+1;test[2]=1}else{test[2]=0;}
                                if(fieldArray[3]!=fieldArray[7] && fieldArray[3]!=fieldArray[2] &&fieldArray[3]!=0){b=b+1;test[3]=1}else{test[3]=0;}
                                if(fieldArray[4]!=fieldArray[0] && fieldArray[4]!=fieldArray[5] && fieldArray[4]!=fieldArray[8] &&fieldArray[4]!=0){b=b+1;test[4]=1}else{test[4]=0;}
                                if(fieldArray[5]!=fieldArray[1] && fieldArray[5]!=fieldArray[6] && fieldArray[5]!=fieldArray[9]  &&fieldArray[5]!=fieldArray[4] &&fieldArray[5]!=0){b=b+1;test[5]=1}else{test[5]=0;}
                                if(fieldArray[6]!=fieldArray[2] && fieldArray[6]!=fieldArray[7] && fieldArray[6]!=fieldArray[10] &&
                                    fieldArray[6]!=fieldArray[5] &&fieldArray[6]!=0){b=b+1;test[6]=1}else{test[6]=0;}
                                if(fieldArray[7]!=fieldArray[3] && fieldArray[7]!=fieldArray[11] && fieldArray[7]!=fieldArray[6] &&fieldArray[7]!=0){b=b+1;test[7]=1}else{test[7]=0;}
                                if(fieldArray[8]!=fieldArray[4] && fieldArray[8]!=fieldArray[9] && fieldArray[8]!=fieldArray[12] &&fieldArray[8]!=0){b=b+1;test[8]=1}else{test[8]=0;}
                                if(fieldArray[9]!=fieldArray[5] && fieldArray[9]!=fieldArray[10] && fieldArray[9]!=fieldArray[13] &&
                                    fieldArray[9]!=fieldArray[8] &&fieldArray[9]!=0){b=b+1;test[9]=1}else{test[9]=0;}
                                if(fieldArray[10]!=fieldArray[6] && fieldArray[10]!=fieldArray[11] && fieldArray[10]!=fieldArray[14] &&
                                    fieldArray[10]!=fieldArray[9] &&fieldArray[10]!=0){b=b+1;test[10]=1}else{test[10]=0;}
                                if(fieldArray[11]!=fieldArray[7] && fieldArray[11]!=fieldArray[15] && fieldArray[11]!=fieldArray[10] &&fieldArray[11]!=0){b=b+1;test[11]=1}else{test[11]=0;}
                                if(fieldArray[12]!=fieldArray[8] && fieldArray[12]!=fieldArray[13] &&fieldArray[12]!=0){b=b+1;test[12]=1}else{test[12]=0;}
                                if(fieldArray[13]!=fieldArray[9] && fieldArray[13]!=fieldArray[14] && fieldArray[13]!=fieldArray[12] &&fieldArray[13]!=0){b=b+1;test[13]=1}else{test[13]=0;}
                                if(fieldArray[14]!=fieldArray[10] && fieldArray[14]!=fieldArray[15] && fieldArray[14]!=fieldArray[13] &&fieldArray[14]!=0){b=b+1;test[14]=1}else{test[14]=0;}
                                if(fieldArray[15]!=fieldArray[11] && fieldArray[15]!=fieldArray[14] &&fieldArray[15]!=0){b=b+1;test[15]=1}else{test[15]=0;}

                                if(b==16){
                                    //game.add.text(300,30,"gameover",{font: "bold 16px Arial", fill: '#ffffff'});}
                                    game_over=true;
                                }}; 
					// starting the tween 
					fadeIn.start();
				},
				
				// GIVING A NUMBER IN A 1-DIMENSION ARRAY, RETURNS THE ROW
				toRow: function (n){
					return Math.floor(n/4);
				},
				
				// GIVING A NUMBER IN A 1-DIMENSION ARRAY, RETURNS THE COLUMN
				toCol: function (n){
					return n%4;	
				},
				
				// THIS FUNCTION UPDATES THE NUMBER AND COLOR IN EACH TILE
				updateNumbers: function (){
					// look how I loop through all tiles
					tileSprites.forEach(function(item){
						// retrieving the proper value to show
						var value = fieldArray[item.pos];
                        // check if 4096
                        if(value == 4096 && alreadyWon==false){play_state.wonStatus();};
						// showing the value
						item.getChildAt(0).text=value;
						// tinting the tile
						item.tint=colors[value];
                        // updating the score
                        if(score>9){
                            if(score<=99){score_x=225};
                            if(score>99 && score<1000){score_x=215};
                            if(score>999 && score<10000){score_x=205};
                            if(score>9999 && score<100000){score_x=195};
                            if(score>99999 && score<1000000){score_x=185};
                        }
                        score_label.destroy();
                        score_label = game.add.text(score_x,65,""+score,{font:"30px Arial",fill:'#645D55'});
                        //Best
                        if(Best>9){
                            if(Best<=99){best_x=320};
                            if(Best>99 && Best<1000){best_x=310};
                            if(Best>999 && Best<10000){best_x=300};
                            if(Best>9999 && Best<100000){best_x=290};
                            if(Best>99999 && Best<1000000){best_x=280};
                        }   
                            if(!Best){Best=0};
                            Best_label.destroy();
                            Best_label = game.add.text(best_x,65,""+Best,{font:"30px Arial",fill:'#645D55',align:"center"});
                        
					});	
				},
				
				// MOVING TILES LEFT
				moveLeft: function (){
					// Is the player allowed to move?
                         if(canMove){
                         	// the player can move, let's set "canMove" to false to prevent moving again until the move process is done
                              canMove=false;
                              // keeping track if the player moved, i.e. if it's a legal move
                              var moved = false;
                              // look how I can sort a group ordering it by a property
     					tileSprites.sort("x",Phaser.Group.SORT_ASCENDING);
     					// looping through each element in the group
     					tileSprites.forEach(function(item){
     						// getting row and column starting from a one-dimensional array
     						var row = play_state.toRow(item.pos);
     						var col = play_state.toCol(item.pos);
     						// checking if we aren't already on the leftmost column (the tile can't move)
     						if(col>0){
     							// setting a "remove" flag to false. Sometimes you have to remove tiles, when two merge into one 
     							var remove = false;
     							// looping from column position back to the leftmost column
     							for(i=col-1;i>=0;i--){
     								// if we find a tile which is not empty, our search is about to end...
     								if(fieldArray[row*4+i]!=0){
     									// ...we just have to see if the tile we are landing on has the same value of the tile we are moving
     									if(fieldArray[row*4+i]==fieldArray[row*4+col]){
     										// in this case the current tile will be removed
     										remove = true;
     										i--;                                             
     									}
     									break;
     								}
     							}
     							// if we can actually move...
     							if(col!=i+1){
     								// set moved to true
                                             moved=true;
                                             // moving the tile "item" from row*4+col to row*4+i+1 and (if allowed) remove it
                                             play_state.moveTile(item,row*4+col,row*4+i+1,remove);
     							}
                                else{

                                }
     						}
     					});

     					// completing the move
     					play_state.endMove(moved);
                         }
                        if(game_over){
                            key_check+=1;
                            if(key_check==2){
                            play_state.gameover()};
                        };                 
				},
				
				// FUNCTION TO COMPLETE THE MOVE AND PLACE ANOTHER "2" IF WE CAN
				endMove: function (m){
					// if we move the tile...
					if(m){
						// add another "2"
     					play_state.addTwo();
                         }
                         else{
                         	// otherwise just let the player be able to move again
						canMove=true;
					}
				},
				
				// FUNCTION TO MOVE A TILE
				moveTile: function (tile,from,to,remove){
					// first, we update the array with new values
                         fieldArray[to]=fieldArray[from];
                         fieldArray[from]=0;
                         tile.pos=to;
                         // then we create a tween
                         var movement = game.add.tween(tile);
                         movement.to({x:tileSize*(play_state.toCol(to))+17,y:tileSize*(play_state.toRow(to))+210},150);
                         if(remove){
                         	// if the tile has to be removed, it means the destination tile must be multiplied by 2
                              fieldArray[to]*=2;
                              // score increases
                              score = score+fieldArray[to];
                              if(score>Best){Best=score};
                              // at the end of the tween we must destroy the tile
                              movement.onComplete.add(function(){
                                   tile.destroy();
                              });
                         }
                         // let the tween begin!
                         movement.start();
                    },
                    
                    // MOVING TILES UP - SAME PRINCIPLES AS BEFORE
                    moveUp: function(){
                          if(canMove){
                              canMove=false;
                              var moved=false;
     					tileSprites.sort("y",Phaser.Group.SORT_ASCENDING);
     					tileSprites.forEach(function(item){
     						var row = play_state.toRow(item.pos);
     						var col = play_state.toCol(item.pos);
     						if(row>0){  
                                        var remove=false;
     							for(i=row-1;i>=0;i--){
     								if(fieldArray[i*4+col]!=0){
     									if(fieldArray[i*4+col]==fieldArray[row*4+col]){
     										remove = true;
     										i--;                                             
     									}
                                                  break
     								}
     							}
     							if(row!=i+1){
                                             moved=true;
                                             play_state.moveTile(item,row*4+col,(i+1)*4+col,remove);
     							}
     						}
     					});

     					play_state.endMove(moved);
                         }
                        if(game_over){
                            key_check+=1;
                            if(key_check==2){
                            play_state.gameover()};
                        };
				},
				
				// MOVING TILES RIGHT - SAME PRINCIPLES AS BEFORE
                    moveRight: function (){
                          if(canMove){
                              canMove=false;
                              var moved=false;
     					tileSprites.sort("x",Phaser.Group.SORT_DESCENDING);
     					tileSprites.forEach(function(item){
     						var row = play_state.toRow(item.pos);
     						var col = play_state.toCol(item.pos);
     						if(col<3){
                                        var remove = false;
     							for(i=col+1;i<=3;i++){
     								if(fieldArray[row*4+i]!=0){
                                                  if(fieldArray[row*4+i]==fieldArray[row*4+col]){
     										remove = true;
     										i++;                                             
     									}
     									break
     								}
     							}
     							if(col!=i-1){
                                             moved=true;
     								play_state.moveTile(item,row*4+col,row*4+i-1,remove);
     							}
     						}
     					});

     					play_state.endMove(moved);
                         }
                        if(game_over){
                            key_check+=1;
                            if(key_check==2){
                            play_state.gameover()};
                        };
                        //if(testing==3){game.add.text(50,20,"DOING_IT",{font:"30px Arial"})};
				},
                    
                    // MOVING TILES DOWN - SAME PRINCIPLES AS BEFORE
                    moveDown: function (){
                          if(canMove){
                              canMove=false;
                              var moved=false;
     					tileSprites.sort("y",Phaser.Group.SORT_DESCENDING);
     					tileSprites.forEach(function(item){
     						var row = play_state.toRow(item.pos);
     						var col = play_state.toCol(item.pos);
     						if(row<3){
                                        var remove = false;
     							for(i=row+1;i<=3;i++){
     								if(fieldArray[i*4+col]!=0){
     									if(fieldArray[i*4+col]==fieldArray[row*4+col]){
     										remove = true;
     										i++;                                             
     									}
                                                  break
     								}
     							}
     							if(row!=i-1){
                                             moved=true;
     								play_state.moveTile(item,row*4+col,(i-1)*4+col,remove);
     							}
     						}
     					});

     				     play_state.endMove(moved);
                         }
                        if(game_over){
                            key_check+=1;
                            if(key_check==2){
                            play_state.gameover()};
                        };
				},

                    wonStatus:function(){
                        alreadyWon=true;
                        orange_display=game.add.sprite(10,200,'won');
                        orange_display.alpha=0.3;
                        //you won
                        youWonText=game.add.text(112,300,"You Won",{font:"bold 42px Arial",fill: 'FF7000'});
                        //keep going
                        button_display=game.add.button(137,400,"keep_going",play_state.destroyWonStatus);
                        button_display.alpha=0.0;
                        keepGoingText=game.add.text(140,400,"Keep going",{font:"bold 24px Arial",fill: '#ffffff'});

                    },
                    destroyWonStatus: function(){
                        orange_display.destroy();
                        youWonText.destroy();
                        button_display.destroy();
                        keepGoingText.destroy();
                    },
                    gameover: function(){
                        game_over=false;
                        key_check=0;
                        if(score==Best){
                            localStorage.setItem('Best', score)};
                        //game over
                        grey_display=game.add.sprite(10,200,'grey');
                        grey_display.alpha=0.7;
                        game.add.text(89,300,"Game Over",{font:"bold 42px Arial",fill: '#ffffff'});
                        //try again
                        button_display=game.add.button(148,400,'try_again',play_state.restart);
                        button_display.alpha=0.0;
                        game.add.text(148,400,"Try again",{font:"bold 24px Arial",fill: '#ffffff'});
                    },

                    restart: function(){
                        game.state.start('play');
                    }
	    		};