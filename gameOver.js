var gameOver_state = {

  create: function() {
    went_gameOver=true;
    this.spacebar = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

    label = game.add.text(game.width / 2 , game.height / 2, 'Score: '+score+'\nGAME OVER\nPress SPACE to restart',
      { font: '22px Lucida Console', fill: '#fff', align: 'center'});
    label.anchor.setTo(0.5, 0.5);
  },

  update: function() {
    score = 0;
    if (this.spacebar.isDown)
      this.game.state.start('play');
  }
};

//<meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1,
 //width=device-width, height=device-height, target-densitydpi=device-dpi" />