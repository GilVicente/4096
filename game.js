// Initialize Phaser
    var game = new Phaser.Game(400, 600, Phaser.CANVAS, '');

                // tile width, in pixels
				var tileSize = 94;
				var tile;
				// creation of a new phaser game, with a proper width and height according to tile size
				var fieldArray = new Array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
				// game array, starts with all cells to zero

				// this is the group which will contain all tile sprites
				var tileSprites
				// variables to handle keyboard input
				var Best = localStorage.getItem('Best');
                var score = 0;
                var score_label;
                var game_over=false;
                //
				var upKey;
				var downKey;
				var leftKey;
				var rightKey;
				var arrow_upKey;
				var arrow_downKey;
				var arrow_leftKey;
				var arrow_rightKey;
				// colors to tint tiles according to their value
				var colors = {
					2:0xFFFFFF,
					4:0xECE0CA,
					8:0xF5B077,
					16:0xF99362,
					32:0xF57D64,
					64:0xF85F3D,
					128:0xF0CD73,
					256:0xECCE5E,
					512:0xEEC83F,
					1024:0xEBC53C,
					2048:0xEDBE28,
					4096:0x3B3B2F,
					8192:0xFF3333,
					16384:0xFF2222,
					32768:0xFF1111,
					65536:0xFF0000
				};
				// at the beginning of the game, the player cannot move
                    var canMove=false;
                //score and best x variables
                var score_x;
                var best_x;
                var key_check=0;
                var testing = 0;
                var out = false;

                var alreadyWon=false;
                var orange_display;
                var youWonText;
                var button_display;
                var keepGoingText;



				// Define all the states
				game.state.add('load',load_state);
				game.state.add('play',play_state); 
  
				// Start with the 'load' state
				game.state.start('load');