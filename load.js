			var load_state = {
				// THE GAME IS PRELOADING
				preload: function() {
					// preload the only image we are using in the game
					game.load.image("tile", "tile.png");
                    game.load.image("grey","Game_Over.png");
                    game.load.image("try_again","Try_Again.png");
                    game.load.image("grid","2048_v2.png");
                    game.load.image("restart","restart_button.png");
                    game.load.image("turn","rotate.png");
                    game.load.image("won","game_won.png");
                    game.load.image("keep_going","keep_going.png");
				},

				create: function(){
					game.state.start('play');
				}
			};